# OSINT Resources 
A list of resources for OSINT. Those resources and tools are intendet only for penetration testers, red teamers and educational use in conrolled environment.

**What is OSINT?**

''Open-source intelligence [(OSINT)](https://en.wikipedia.org/wiki/Open-source_intelligence) is a multi-factor (qualitative, quantitative) methodology for collecting, analyzing and making decisions about data accessible in publicly available sources to be used in an intelligence context. In the intelligence community, the term "open" refers to overt, publicly available sources (as opposed to covert or clandestine sources). OSINT under one name or another has been around for hundreds of years. With the advent of instant communications and rapid information transfer, a great deal of actionable and predictive intelligence can now be obtained from public, unclassified sources. It is not related to open-source software or collective intelligence.

OSINT is the collection and analysis of information that is gathered from public, or open, sources. OSINT is primarily used in national security, law enforcement, and business intelligence functions and is of value to analysts who use non-sensitive intelligence in answering classified, unclassified, or proprietary intelligence requirements across the previous intelligence disciplines.''

___________
|Table of contents
===================
1. Notice for OSINT tools
2. Bookmark list of OSINT resources
3. List of OSINT resources from another list
4. Contribution


| Notice for OSINT tools
==========================
General rule for OSINT tools is that they expire quite offten. You have to be aware that some tools for now but they might not work tomorow. Also there are some tools that work for long time, so when using some of these tools, and it doesn't work, be aware that they might be expired. 

|Bookmark list of OSINT resources
====================================
On our website (https://abstract-security.lugons.org/), under Resources you have folder OSINT where there is html file called Bookmarks which you can import in your browser and have many tools as bookmarks in your browser. 

|List of OSINT resources from another list
=============================================
On GitHub, one user has curated list of OSINT tools that is usefull. As the list is long i will not paste it here so i will just link it and if you are interested go and check it out - https://github.com/jivoi/awesome-osint

Another great resource for OSINT is this blog made by a guy who does OSINT professionally -> https://ohshint.gitbook.io/oh-shint-its-a-blog/

| Contribution
=================
Every contribution is welcome! If you need more info on something check our website (https://abstract-security.lugons.org/), our Discord (https://discord.gg/zn3wsrr) or contact me (noman29@lugons.org).

Also, we have some members that regulary post OSINT tools on our Discord Server, so if you want to chat about them or want to see what others are using, come join us.
